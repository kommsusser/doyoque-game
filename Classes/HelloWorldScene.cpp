/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

//////////////////////////////////////////////////////////////////////////////
// at this file i covered
// 1. implement resource to the game.
// 2. add touch event listener.
// 3. collision detection and physics. 
//////////////////////////////////////////////////////////////////////////////

#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

enum class PhysicsCategory
{
  None = 0,
  Monster = (1 << 0), //1
  Projectile = (1 << 1), //2
  All = PhysicsCategory::Monster | PhysicsCategory::Projectile //3
};

Scene* HelloWorld::createScene()
{
  auto scene = Scene::createWithPhysics();
  scene->getPhysicsWorld()->setGravity(Vec2(0,0));
  scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);

  auto layer = HelloWorld::create();

  scene->addChild(layer);

  return scene;
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
  //////////////////////////////
  // 1. super init first
  // we call super Class init method
  // only if this succeeds do you procceed with HelloWorldScene setup.
  if ( !Scene::init() )
  {
      return false;
  }

  /////////////////////////////
  // 2. you then get the window's bound using the game Director singleton.
  auto origin = Director::getInstance()->getVisibleOrigin();
  auto winSize = Director::getInstance()->getVisibleSize();

  /////////////////////////////
  // 3. create DrawNode to draw a gray rectangle that fill the screen
  // and add it to the scene.
  auto background = DrawNode::create();
  background->drawSolidRect(origin, winSize, Color4F(0.6,0.6,0.6,2.0));
  this->addChild(background);

  ////////////////////////////
  // 4. create player sprite
  // _player variable comes from HelloWorldScene.h at private method inside the Class
  _player = Sprite::create("player.png");
  _player->setPosition(Vec2(winSize.width * 0.1, winSize.height * 0.5));
  this->addChild(_player);

  ////////////////////////////
  // make monster spawn continously
  srand((unsigned int)time(nullptr));
  this->schedule(schedule_selector(HelloWorld::addMonster), 1.5);

  ////////////////////////////
  // Add event linstener for clicking
  // side note
  // *EventListenerTouchOneByOne this type calls your callback method for once for each touch event
  // *EventListenerTouchAllAtOnce this type calls your callback method with all of the touch event
  // touch event support four callbacks
  // onTouchBegan -> called when your finger first touch the screen. You must return true if using EventListenerTouchOneByOne. 
  // onTouchMoved -> called when your finger already touch the screen move without lifting off the screen.
  // onTouchEnded -> called when your finger lift off the screen.
  // onTouchCancelled -> called in certain circumtstances that stop event handling.
  auto eventListener = EventListenerTouchOneByOne::create();
  eventListener->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);
  this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(eventListener, _player);

  return true;
}


void HelloWorld::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif

    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/

    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);


}

void HelloWorld::addMonster(float dt)
{
  // create monster instance with resource monster.png
  auto monster = Sprite::create("monster.png");

  //1
  auto monsterSize = monster->getContentSize();
  auto physicsBody = PhysicsBody::createCircle(monsterSize.width/3);

  //2
  physicsBody->setDynamic(true);

  //3
  physicsBody->setCategoryBitmask((int)PhysicsCategory::Monster);
  physicsBody->setCollisionBitmask((int)PhysicsCategory::None);
  physicsBody->setContactTestBitmask((int)PhysicsCategory::Projectile);

  monster->setPhysicsBody(physicsBody);

  //////////////////////////////////////////
  // 1. set the monster size
  // getContentSize() means get the actual size from assets
  // it set Y-position to a random value to keep things interesting 
  auto monsterContentSize = monster->getContentSize();
  auto selfContentSize = this->getContentSize();
  int minY = monsterContentSize.height/2;
  int maxY = selfContentSize.height - monsterContentSize.height/2;
  int rangeY = maxY - minY;
  int randomY = (rand() % rangeY) + minY;

  monster->setPosition(Vec2(selfContentSize.width + monsterContentSize.width/2, randomY));
  this->addChild(monster);

  //////////////////////////////////////////
  // 2. calculate a random duration between 2 and 4 second
  // for the action.
  int minDuration = 2.0;
  int maxDuration = 4.0;
  int rangeDuration = maxDuration - minDuration;
  int randomDuration = (rand() % rangeDuration) + minDuration;

  //////////////////////////////////////////
  // 3. finally create action that move the monster across the screen
  // from right to left and instructs the monster to run it.
  auto actionMove = MoveTo::create(randomDuration, Vec2(-monsterContentSize.width/2, randomY));
  auto actionRemove = RemoveSelf::create();
  monster->runAction(Sequence::create(actionMove, actionRemove, nullptr));
}

bool HelloWorld::onTouchBegan(Touch *touch, Event *unused_event)
{
  //////////////////////////////////////////
  // 1. -how to get _player object
  // auto node = unused_event->getCurrentTarget();

  //////////////////////////////////////////
  // 2. get coordinate of the touch within the scene's
  // coordinate system and then calculate the offset of this point from the current player position.
  Vec2 touchLocation = touch->getLocation();
  Vec2 offset = touchLocation - _player->getPosition();

  //////////////////////////////////////////
  // 3. offset.x value is negative, it means the player is ready to shoot backwards
  // in this case it not allowed.
  if (offset.x < 0)
  {
    return true;
  }

  //////////////////////////////////////////
  // 4. at line 200, 201, and 210 is create the projectile from assets.
  auto projectile = Sprite::create("projectile.png");
  projectile->setPosition(_player->getPosition());
  auto projectileSize = projectile->getContentSize();
  auto physicsBody = PhysicsBody::createCircle(projectileSize.width/2);
  physicsBody->setDynamic(true);
  physicsBody->setCategoryBitmask((int)PhysicsCategory::Projectile);
  physicsBody->setCollisionBitmask((int)PhysicsCategory::None);
  physicsBody->setContactTestBitmask((int)PhysicsCategory::Monster);
  projectile->setPhysicsBody(physicsBody);

  this->addChild(projectile);

  //////////////////////////////////////////
  // 5. call normalize() to convert the offset into a unit vector
  // 1000 is enough to extend the past the edge of your screen at this resolution.
  offset.normalize();
  auto shootAmount = offset * 1000;

  //////////////////////////////////////////
  // 6. Adding vector projectile position gives you the target position.
  auto realDest = shootAmount + projectile->getPosition();

  //////////////////////////////////////////
  // 7. move the projectile to target position that you clicked.
  auto actionMove = MoveTo::create(2.0f, realDest);
  auto actionRemove = RemoveSelf::create();
  projectile->runAction(Sequence::create(actionMove, actionRemove, nullptr));

  return true;
}