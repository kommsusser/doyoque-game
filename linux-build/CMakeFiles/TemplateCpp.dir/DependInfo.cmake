# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/doyoque/Documents/cocosProject/MyGame/Classes/AppDelegate.cpp" "/home/doyoque/Documents/cocosProject/MyGame/linux-build/CMakeFiles/TemplateCpp.dir/Classes/AppDelegate.cpp.o"
  "/home/doyoque/Documents/cocosProject/MyGame/Classes/HelloWorldScene.cpp" "/home/doyoque/Documents/cocosProject/MyGame/linux-build/CMakeFiles/TemplateCpp.dir/Classes/HelloWorldScene.cpp.o"
  "/home/doyoque/Documents/cocosProject/MyGame/proj.linux/main.cpp" "/home/doyoque/Documents/cocosProject/MyGame/linux-build/CMakeFiles/TemplateCpp.dir/proj.linux/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CC_USE_JPEG=1"
  "CC_USE_NAVMESH=1"
  "CC_USE_PNG=1"
  "CC_USE_TIFF=1"
  "LINUX"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../cocos2d"
  "../cocos2d/cocos"
  "../cocos2d/deprecated"
  "../cocos2d/cocos/platform"
  "../cocos2d/extensions"
  "../cocos2d/external"
  "../cocos2d/external/uv/include"
  "../cocos2d/external/glfw3/include/linux"
  "../Classes"
  "../cocos2d/cocos/audio/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/doyoque/Documents/cocosProject/MyGame/linux-build/engine/cocos/core/CMakeFiles/cocos2d.dir/DependInfo.cmake"
  "/home/doyoque/Documents/cocosProject/MyGame/linux-build/engine/external/unzip/CMakeFiles/unzip.dir/DependInfo.cmake"
  "/home/doyoque/Documents/cocosProject/MyGame/linux-build/engine/external/tinyxml2/CMakeFiles/tinyxml2.dir/DependInfo.cmake"
  "/home/doyoque/Documents/cocosProject/MyGame/linux-build/engine/external/flatbuffers/CMakeFiles/flatbuffers.dir/DependInfo.cmake"
  "/home/doyoque/Documents/cocosProject/MyGame/linux-build/engine/external/xxhash/CMakeFiles/xxhash.dir/DependInfo.cmake"
  "/home/doyoque/Documents/cocosProject/MyGame/linux-build/engine/external/recast/CMakeFiles/recast.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
